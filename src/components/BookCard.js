import {Container, Card, Button, Row, Col} from "react-bootstrap";
import { Link } from "react-router-dom";
export default function BookCard({bookProp}) {


	const { _id, title, author, genre, price } = bookProp;


    return (
        <Container className="mt-5">
            <Row>
                <Col lg={{ span: 8, offset: 2 }}>
                    <Card bordered className="shadow-lg shadow-sm">
                        <Card.Body className="text-center">
                            <Card.Title>
                                {title}
                            </Card.Title>
                            <Card.Subtitle>
                                Author:
                            </Card.Subtitle>
                            <Card.Text>
                                {author}
                            </Card.Text>
                            <Card.Subtitle>
                                Genre:
                            </Card.Subtitle>
                            <Card.Text>
                                {genre}
                            </Card.Text>
                            <Card.Subtitle>
                                Price:
                            </Card.Subtitle>
                            <Card.Text>
                                Php {price}
                            </Card.Text>
                            <Button as={Link} to={`/books/${_id}`} variant="success">Add to Cart</Button>
                        </Card.Body>        
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}