import { useContext } from "react"
import { NavLink, Link } from "react-router-dom";
import logo from './logo.jpg';

import { Container, Nav, Navbar } from "react-bootstrap";
import UserContext from "../UserContext";

export default function AppNavbar(){

    const { user } = useContext(UserContext);
    console.log(user);
    return(
        <Navbar className="navbar" expand="lg" variant="success">
        <Container fluid>
            
             {/*<img src={logo} alt="Logo" className="w-1 object-cover"/>*/}
            

            <Navbar.Brand className="text-white " style={{fontSize:"30px"}} as={ NavLink } to="/">DigiBooks</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto">
                <Nav.Link className="text-white mx-3" style={{fontSize:"20px"}} as={ NavLink } to="/" end>Home</Nav.Link>
                <Nav.Link className="text-white mx-3" style={{fontSize:"20px"}} as={ NavLink } to="/books" end>Books</Nav.Link>
                {
                    (user.id === null)
                    ?
                    <>
                        <Nav.Link className="text-white mx-3" style={{fontSize:"20px"}} as={ NavLink } to="/login" end>Login</Nav.Link>
                        <Nav.Link className="text-white mx-3" style={{fontSize:"20px"}} as={ NavLink } to="/register" end>Register</Nav.Link>
                    </>   
                    :
                    <Nav.Link className="text-white mx-3" style={{fontSize:"20px"}} as={ NavLink } to="/logout" end>Logout</Nav.Link>
                    
                }
            </Nav>
            </Navbar.Collapse>
        </Container>
        </Navbar>
    )
}