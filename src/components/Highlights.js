import {Col, Row, Card} from "react-bootstrap"
import library from './library.jpg';
import ebook from './ebook.jpg';
import sale from './sale.jpg';

export default function Highlights(){
    return(
        <Row className="my-3">
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3 text-center">
                    <Card.Body>
                        <Card.Title>
                            <img src={library} alt="Library" className="w-100 object-cover"/>
                            <h2 className="pb-3">Library of your Favorite Books</h2>
                        </Card.Title>
                        <Card.Text>
                            We have millions of book titles you can choose from across all genre of your choice.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3 text-center">
                    <Card.Body>
                        <Card.Title>
                            <img src={ebook} alt="Ebook" className="w-100 object-cover"/>
                            <h2 className="pb-3">Electronic Books & Audio</h2>
                        </Card.Title>
                        <Card.Text>
                            An assortment of downloadable titles for our e-reader and downloadable audio recordings.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3 text-center">
                    <Card.Body>
                        <Card.Title>
                        <img src={sale} alt="Sale" className="w-100 object-cover"/>
                            <h2 className="pb-3">Join our BookStar Community</h2>
                        </Card.Title>
                        <Card.Text>
                            Subscribe to our BookStar membership and gain discounts on your next purchases!
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}