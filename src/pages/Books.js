import { useEffect, useState } from "react";
import BookCard from "../components/BookCard";
import { Row, Container } from 'react-bootstrap';


export default function Books() {


	const [books, setBooks] = useState([]); 

	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/books/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setBooks(data.map(book =>{
				return(
					<BookCard key={book._id} bookProp={book}/>
				);
			}));
		})
	}, []);


	return(
		<>
		<Container>
			<Row>
			<h1 className="mt-5 mb-3" >Books</h1>
			</Row>
		</Container>
			{books}
		</>
	)
}



