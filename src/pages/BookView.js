import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import {Container, Card, Button, Row, Col} from "react-bootstrap";
import { useParams, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

export default function BookView(){

	const { bookId } = useParams();

	const navigate = useNavigate();
	const [title, setTitle] = useState('');
	const [author, setAuthor] = useState('');
	const [genre, setGenre] = useState('');
	const [price, setPrice] = useState(0);

	useEffect(()=>{
		console.log(bookId);

		fetch(`${ process.env.REACT_APP_API_URL }/books/${bookId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			
			setTitle(data.title);
			setAuthor(data.author);
			setGenre(data.genre);
			setPrice(data.price);

		});

	}, [bookId])


	const orders = (bookId) =>{

		fetch(`${ process.env.REACT_APP_API_URL }/users/order`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				bookId: bookId
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data){
				Swal.fire({
					title: "Successfully Ordered",
					icon: "success",
					text: "You have successfully ordered this book."
				});

				navigate("/books");
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}

		});

	}

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card bordered className="shadow-lg shadow-sm">
						<Card.Body className="text-center">
							<Card.Title>{title}</Card.Title>
							<Card.Subtitle>Author: </Card.Subtitle>
							<Card.Text>{author}</Card.Text>
							<Card.Subtitle>Genre:</Card.Subtitle>
							<Card.Text>{genre}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							<Button variant="success"  size="lg" onClick={() => orders(bookId)}>Buy Now</Button>
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
}